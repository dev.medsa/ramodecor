<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }
    
    public function category()
    {
        return view('frontend.category');
    }
    
    public function sample()
    {
        return view('frontend.sample');
    }

    public function blog()
    {
        return view('frontend.blog');
    }
    public function post()
    {
        return view('frontend.content-blog');
    }
    public function contact()
    {
        return view('frontend.contact');
    }
}
