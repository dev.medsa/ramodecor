<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app(
            'App\Repositories\Category\ICategoryInterface',
            'App\Repositories\Category\CategoryRepository'
        );
        $this->app(
            'App\Repositories\Comment\ICommentInterface',
            'App\Repositories\Comment\CommentRepository'
        );
        $this->app(
            'App\Repositories\Post\IPostInterface',
            'App\Repositories\Post\PostRepository'
        );
        $this->app(
            'App\Repositories\Tag\ITagInterface',
            'App\Repositories\Tag\TagRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
