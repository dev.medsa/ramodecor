<?php

namespace App\Repositories\Tag;

interface ITagInterface
{

    public function all();
    public function find( int $tag_id );
    public function delete( int $tag_id );
    public function create( array $tag_data );
    public function update( int $tag_id , array $tag_data );

}