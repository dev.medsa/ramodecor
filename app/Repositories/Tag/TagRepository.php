<?php

namespace App\Repositories\Tag;

use App\Models\Tag;

class TagRepository implements ITagInterface
{
    protected $tag ;

    public function __construct( Tag $tag)
    {
        $this->tag = $tag;
    }

    public function all()
    {
        return $this->tag::all();
    }

    public function find( int $tag_id )
    {
        return $this->tag::find($tag_id);
    }

    public function delete( int $tag_id )
    {
        return $this->tag::destroy($tag_id);
    }

    public function create( array $tag_data )
    {
        return $this->tag::create($tag_data);
    }

    public function update( int $tag_id , array $tag_data )
    {
        $find = $this->tag::find($tag_data);
        return $find::update($tag_data);
    }
    
    public function __call($method,$args)
    {
        return call_user_func_array([$this->tag,$method],$args);
    }

}
