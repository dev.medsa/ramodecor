<?php

namespace App\Repositories\Comment;

use App\Models\Comment;

class CommentRepository implements ICommentInterface
{
    protected $comment ;

    public function __construct( Comment $comment)
    {
        $this->comment = $comment;
    }

    public function all()
    {
        return $this->comment::all();
    }

    public function find( int $comment_id )
    {
        return $this->comment::find($comment_id);
    }

    public function delete( int $comment_id )
    {
        return $this->comment::destroy($comment_id);
    }

    public function create( array $comment_data )
    {
        return $this->comment::create($comment_data);
    }

    public function update( int $comment_id , array $comment_data )
    {
        $find = $this->comment::find($comment_data);
        return $find::update($comment_data);
    }
    
    public function __call($method,$args)
    {
        return call_user_func_array([$this->comment,$method],$args);
    }

}
