<?php

namespace App\Repositories\Comment;

interface ICommentInterface
{

    public function all();
    public function find( int $comment_id );
    public function delete( int $comment_id );
    public function create( array $comment_data );
    public function update( int $comment_id , array $comment_data );

}