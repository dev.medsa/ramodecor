<?php

namespace App\Repositories\Post;

use App\Models\Post;

class PostRepository implements IPostInterface
{
    protected $post ;

    public function __construct( Post $post)
    {
        $this->post = $post;
    }

    public function all()
    {
        return $this->post::all();
    }

    public function find( int $post_id )
    {
        return $this->post::find($post_id);
    }

    public function delete( int $post_id )
    {
        return $this->post::destroy($post_id);
    }

    public function create( array $post_data )
    {
        return $this->post::create($post_data);
    }

    public function update( int $post_id , array $post_data )
    {
        $find = $this->post::find($post_data);
        return $find::update($post_data);
    }
    
    public function __call($method,$args)
    {
        return call_user_func_array([$this->post,$method],$args);
    }

}
