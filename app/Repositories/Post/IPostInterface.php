<?php

namespace App\Repositories\Post;

interface IPostInterface
{

    public function all();
    public function find( int $post_id );
    public function delete( int $post_id );
    public function create( array $post_data );
    public function update( int $post_id , array $post_data );

}