<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace'=>'Front'],function(){
    Route::get('/', 'FrontController@index')->name('frontend.index');
    Route::get('/category', 'FrontController@category')->name('frontend.category');
    Route::get('/sample', 'FrontController@sample')->name('frontend.sample');
    Route::get('/blog', 'FrontController@blog')->name('frontend.blog');
    Route::get('/post', 'FrontController@post')->name('frontend.content.blog');
    Route::get('/contact', 'FrontController@contact')->name('frontend.contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{path}', 'HomeController@index')->where(['path'=>'.*']);
