@include('partials.front.header')

{{-- <!--------------------hero section starts here---------------------->
<div class="hero" id="hero">
    <h1>RamoDecor</h1>
</div>
<div class="scroll-down"></div>
<br><br>
<div class="scroll-down-2"></div>
<!--------------------hero section ends here-----------------------> --}}
@yield('content')

@include('partials.front.footer')
<script type="text/javascript">
    new window.hoverEffect({
        parent: document.querySelector('.hero'),
        image1: "/images/decor.jpg",
        image2: "/images/decor-2.jpg",
        displacementImage: '/images/heightMap.png'
    })
</script>
</body>

</html>