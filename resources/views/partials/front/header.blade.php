<!DOCTYPE html>
<html class="no-js csstransforms csstransforms3d csstransitions" lang="en">
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Basic Page Needs
	================================================== -->

    <title>Ramo Decor</title>
    <meta name="description" content="Professional Creative Template">

    <!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#212121">
    <meta name="msapplication-navbutton-color" content="#212121">
    <meta name="apple-mobile-web-app-status-bar-style" content="#212121">

    <!-- CSS
	================================================== -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- csrf
        ================================================== -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- js
        ================================================== -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            $(document).ready(function(){
                var isMenuOpen = false;
                $('.menu-icon').on('click',

                        function (e){
                            $('.social-contact').stop();
                            if(!isMenuOpen){
                                isMenuOpen = true;
                                $('.social-contact').animate({'opacity':'1'},5000, function(){
                                });
                            }
                            else {
                                isMenuOpen = false;
                                $('.social-contact').animate({'opacity':'0'},1, function(){
                            });
                        }
                    }
                );

                
                });
        </script>
    <!-- Favicons
	================================================== -->
    {{-- <link rel="icon" type="image/png" href="https://ivang-design.com/euthenia/favicon.jpg">
    <link rel="apple-touch-icon" href="https://ivang-design.com/euthenia/apple-touch-icon.jpg">
    <link rel="apple-touch-icon" sizes="72x72" href="https://ivang-design.com/euthenia/apple-touch-icon-72x72.jpg">
    <link rel="apple-touch-icon" sizes="114x114" href="https://ivang-design.com/euthenia/apple-touch-icon-114x114.jpg"> --}}

</head>

<body class="over-hide">

    <!-- Page preloader wrap
	================================================== -->

    <div class="animsition" >

        <!-- Nav and Logo
		================================================== -->

        <header class="cd-header">
            <div class="header-wrapper">
                <div class="logo-wrap">
                    <a href="https://ivang-design.com/euthenia/index.html" class="hover-target animsition-link"><img
                            src="./Euthenia_files/logo.png" alt="" data-insta_upload_ext_elem="1"></a>
                </div>
                <div class="nav-but-wrap">
                    <div class="menu-icon hover-target">
                        <span class="menu-icon__line menu-icon__line-left"></span>
                        <span class="menu-icon__line"></span>
                        <span class="menu-icon__line menu-icon__line-right"></span>
                    </div>
                </div>
            </div>
        </header>

        <div class="nav">
            <div class="nav__content">
                <div class="curent-page-name-shadow">Ramo Decor</div>
                <div class="d-flex justify-content-between">
                        <div class="col-6 text-center">
                            <div class="social-contact">
                                    <i class="fa fa-phone text-white hover-target "></i> : <span class="text-secondary">&nbsp;&nbsp;+989222362414</span>
                                    <br><br>
                                    <i class="fa fa-envelope text-white "></i>: <a href="#" class="text-secondary">&nbsp;&nbsp;ramin@gmail.com</a>
                                    <br><br>
                                    <div class="d-flex justify-content-center">
                                            <a href="https://ivang-design.com/euthenia/index.html#" class="linkdin"><i class="fa fa-linkedin"></i></a>
                                            <a href="https://ivang-design.com/euthenia/index.html#" class="instagram"><i class="fa fa-instagram mx-2 "></i></a>
                                            <a href="https://ivang-design.com/euthenia/index.html#" class="telegram"><i class="fa fa-telegram"></i></a>
                                    </div>
                            </div>
                            </div>
                            <div class="col-6">
                                <ul class="nav__list">
                                
                                    <li class="nav__list-item">
                                        <a href="{{ route('frontend.index', []) }}"
                                            class="hover-target animsition-link">خانه</a>
                                    </li>
                                    <li class="nav__list-item ">
                                        <a data-toggle="collapse" href="#collapseSub" class="hover-target collapsed" role="button" aria-expanded="true" aria-controls="collapseSub">نمونه کارها</a>
                                        <ul class="sub-links collapse" id="collapseSub" style="font-size: 9px">
                                            <li><a href="index.html" class="hover-target animsition-link">دکوراسیون داخلی</a></li>
                                            <li><a href="index-1.html" class="hover-target animsition-link">تعمیرات مبلمان</a></li>
                                        </ul>
                                    </a>
                                    <li class="nav__list-item"><a href="{{ route('frontend.blog', []) }}"
                                            class="hover-target animsition-link">بلاگ</a></li>
                                    <li class="nav__list-item"><a href="{{ route('frontend.contact', []) }}"
                                            class="hover-target animsition-link">راه های ارتباطی</a></li>
                                </ul>
                </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $('li.nav__list-item a').click(function(e){
                    e.preventDefault();
                    // if ($(this).siblings('li').find('ul.submenu:visible').length) {
                    //     $('ul.submenu').slideUp('normal');
                    // }
                    // $(this).find('ul.submenu').slideToggle('normal');
                });
        </script>