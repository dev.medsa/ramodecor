        <!-- Social links
		================================================== -->

        <div class="social-fixed">
            <a href="https://ivang-design.com/euthenia/index.html#" ><i class="fa fa-linkedin linkdin"></i></a>
            <a href="https://ivang-design.com/euthenia/index.html#" ><i class="fa fa-instagram instagram"></i></a>
            <a href="https://ivang-design.com/euthenia/index.html#" ><i class="fa fa-telegram telegram"></i></a>
        </div>
        <div class="copyr">
            2019 © <a href="https://themeforest.net/user/ig_design/portfolio" class="logo">Euthenia</a>
        </div>
        <!-- Page cursor
            ================================================== -->
            <div class="scroll-to-top hover-target active-arrow"></div>
        <script>
        if ($('body').hasClass('over-hide')) {
            $('.scroll-to-top').addClass('d-none');
        }
        </script>
        <div class="cursor" id="cursor" style="left: 219px; top: 837px;"></div>
        <div class="cursor2" id="cursor2" style="left: 219px; top: 837px;"></div>
        <div class="cursor3" id="cursor3" style="left: 219px; top: 837px;"></div>

    </div>


    <!-- JAVASCRIPT
    ================================================== -->
    <div class="fit-vids-style" id="fit-vids-style" style="display: none;">­
    <style>
            .fluid-width-video-wrapper {
                width: 100%;
                position: relative;
                padding: 0;
            }

            .fluid-width-video-wrapper iframe,
            .fluid-width-video-wrapper object,
            .fluid-width-video-wrapper embed {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
    </style>
    </div>
    
    <script src="{{ asset('js/plugins.js') }}"></script>
    @stack('script')
    <script src="{{ asset('js/custom.js') }}"></script>
    <!-- End Document
================================================== -->



