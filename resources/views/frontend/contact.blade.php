@extends('layouts.frontend')
@section('content')
<div class="shadow-title parallax-top-shadow" style="top: 0px;">Hola</div>
		
		<div class="section padding-page-top padding-bottom over-hide z-bigger">
			<div class="container z-bigger">
				<div class="row page-title px-5 px-xl-2">
					<div class="col-12 parallax-fade-top" style="top: 0px; opacity: 1;">
						<h1>Say Hello</h1>
					</div>
					<div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 0px; opacity: 1;">
						<p>get in touch</p>
					</div>
				</div>
			</div>
		</div>

		<div class="section padding-bottom-big over-hide z-bigger">
			<div class="container">
				<div class="row px-5 px-xl-2">
					<div class="col-lg-9">
						<div class="row ajax-form">
							<div class="col-md-6">
								<input class="hover-target" name="name" type="text" placeholder="Your Name: *" autocomplete="off">
							</div>	
							<div class="col-md-6 mt-4 mt-md-0">
								<input class="hover-target" name="email" type="email" placeholder="E-Mail: *" autocomplete="off">
							</div>	
							<div class="col-md-6 mt-4">
								<input class="hover-target" name="phone" type="tel" placeholder="Phone Number:" autocomplete="off">
							</div>	
							<div class="col-md-6 mt-4">
								<input class="hover-target" name="location" type="text" placeholder="City, Country:" autocomplete="off">
							</div>	
							<div class="col-md-6 mt-4 hover-target">
								<select name="budget" class="wide" style="display: none;">
									<option data-display="Budget">Budget</option>
									<option value="1">$2000 - $5000</option>
									<option value="2">$5000 - $10000</option>
									<option value="3">$10000 - $25000</option>
									<option value="4">$25000 - $50000</option>
									<option value="5">$50000 and up</option>
								</select><div class="nice-select wide" tabindex="0"><span class="current">Budget</span><ul class="list"><li data-value="Budget" data-display="Budget" class="option selected focus">Budget</li><li data-value="1" class="option">$2000 - $5000</li><li data-value="2" class="option">$5000 - $10000</li><li data-value="3" class="option">$10000 - $25000</li><li data-value="4" class="option">$25000 - $50000</li><li data-value="5" class="option">$50000 and up</li></ul></div>
							</div>	
							<div class="col-md-6 mt-4 hover-target">
								<select name="interested" class="wide" style="display: none;">
									<option data-display="Interested For">Interested For</option>
									<option value="1">Web Design</option>
									<option value="2">Photography</option>
									<option value="3">Branding</option>
									<option value="4">Illustration</option>
									<option value="5">Motion Graphics</option>
									<option value="6">Graphic Design</option>
								</select><div class="nice-select wide" tabindex="0"><span class="current">Interested For</span><ul class="list"><li data-value="Interested For" data-display="Interested For" class="option selected focus">Interested For</li><li data-value="1" class="option">Web Design</li><li data-value="2" class="option">Photography</li><li data-value="3" class="option">Branding</li><li data-value="4" class="option">Illustration</li><li data-value="5" class="option">Motion Graphics</li><li data-value="6" class="option">Graphic Design</li></ul></div>
							</div>	
							<div class="col-md-6 mt-4">
								<input class="hover-target" name="name" type="text" placeholder="Company Name:" autocomplete="off">
							</div>	
							<div class="col-md-6 mt-4">
								<input class="hover-target" name="name" type="text" placeholder="Website:" autocomplete="off">
							</div>	
							<div class="col-md-12 mt-4">
								<textarea class="hover-target" name="message" placeholder="Tell Us Everything *"></textarea>
							</div>
							<div class="col-md-12 mt-4">
								<button class="hover-target" id="send" data-lang="en"><span>submit</span></button>
							</div>
						</div>	
					</div>
					<div class="col-lg-3 mt-4 mt-lg-0">
						<div class="section background-dark-4 p-4">
							<h6 class="mb-3">Call Us</h6>
							<p class="mb-0">+1 423 389 5987</p>
							<p class="mb-4">+1 423 389 5984</p>
							<h6 class="mb-3">Visit Us</h6>
							<p class="mb-4">99-81 middagh st, Brooklyn<br>NY 11201, USA</p>
							<h6 class="mb-3">Email Us</h6>
							<p class="mb-0">office@euthenia.com</p>
							<p class="mb-0">support@euthenia.com</p>
						</div>
					</div>
				</div>		
			</div>				
		</div>

		<div class="section contact-map padding-top-bottom-big over-hide z-bigger">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center z-bigger">
						<h5>99-81 middagh st, Brooklyn<br>NY 11201, USA</h5>
					</div>
					<div class="col-md-12 mt-5 text-center">
						<a href="https://www.google.com/maps/place/99-81+Middagh+St,+Brooklyn,+NY+11201,+USA/@40.6999552,-73.9936128,17z/data=!3m1!4b1!4m5!3m4!1s0x89c25a36fb73a3d1:0xb653e2f02fd08084!8m2!3d40.6999552!4d-73.9914241" class="contact-link hover-target" target="_blank">find us on map</a>
					</div>
				</div>
			</div>		
		</div>

		<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
			<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">office@euthenia.com</div>
			<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="2" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
				<div class="row">
					<div class="col-12 text-center z-bigger py-5">
						<div class="footer-lines">
							<a href="mailto:office@euthenia.com" class="hover-target"><h4>Email Us</h4></a>
						</div>
					</div>
				</div>
			</div>		
		</div>	
		


<script>
    $("body").removeClass("over-hide");
</script>
@endsection
