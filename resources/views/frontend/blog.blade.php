@extends('layouts.frontend')
@section('content')

<div class="shadow-title parallax-top-shadow" style="top: -77.5px;">بلاگ</div>

<div class="section padding-page-top padding-bottom over-hide z-bigger">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12 parallax-fade-top" style="top: 77.5px; opacity: 0.483333;">
                <h1>خبر های روانه</h1>
            </div>
            <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 77.5px; opacity: 0.483333;">
                <p>we do magic</p>
            </div>
        </div>
    </div>
</div>

<div class="section padding-bottom-big z-bigger over-hide">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12">
                <ul class="case-study-wrapper vertical-blog">
                    <li class="case-study-name mb-5">
                        <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                            <h4 class="mb-3">We want to share with you our mood after selection.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>22.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="case-study-name mb-5">
                        <a href="{{ route('frontend.content.blog', []) }}" class="hover-target animsition-link">
                            <h4 class="mb-3">Sleep, code, eat, travel. Repeat.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>21.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="case-study-name mb-5">
                        <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                            <h4 class="mb-3">Don’t get lost quoting your next projects.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>19.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="case-study-name mb-5" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
                        data-scroll-reveal-id="1" data-scroll-reveal-initialized="true"
                        data-scroll-reveal-complete="true">
                        <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                            <h4 class="mb-3">Don´t give up, keep on focus all the time.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>17.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="case-study-name mb-5" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
                        data-scroll-reveal-id="2" data-scroll-reveal-initialized="true"
                        data-scroll-reveal-complete="true">
                        <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                            <h4 class="mb-3">Moments from a life. Day of photography.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>15.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="case-study-name mb-5" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
                        data-scroll-reveal-id="3" data-scroll-reveal-initialized="true"
                        data-scroll-reveal-complete="true">
                        <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                            <h4 class="mb-3">The golden rule of modern webdesign.</h4>
                        </a>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="pl-0 pl-md-5 mb-4">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem accusantium doloremque laudantium...</p>
                                <p class="lead pl-0 pl-md-5 mb-0"><em>14.01.2019.</em></p>
                                <a href="https://ivang-design.com/euthenia/post.html"
                                    class="hover-target animsition-link">
                                    <div class="go-to-post"></div>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Blog background images
		================================================== -->

<ul class="case-study-images">
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/1.jpg')"></div>
    </li>
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/2.jpg');"></div>
    </li>
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/3.jpg');"></div>
    </li>
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/4.jpg');"></div>
    </li>
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/5.jpg');"></div>
    </li>
    <li class="">
        <div class="img-hero-background blog-back-image" style="background-image: url('/images/blog/6.jpg');"></div>
    </li>
</ul>

<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
    <div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
        data-scroll-reveal-id="7" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">Our Stories
    </div>
    <div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="8"
        data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
        <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines">
                    <a href="https://ivang-design.com/euthenia/blog.html" class="hover-target animsition-link">
                        <h4>Older News</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        $("body").removeClass("over-hide");
</script>
@endsection

