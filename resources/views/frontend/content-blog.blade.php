@extends('layouts.frontend')
@section('content')
<div class="shadow-title parallax-top-shadow" style="top: 0px;">Stories</div>

<div class="section padding-page-top padding-bottom over-hide z-bigger">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12 parallax-fade-top" style="top: 0px; opacity: 1;">
                <h1>Sleep, code, travel.</h1>
            </div>
            <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 0px; opacity: 1;">
                <p>by Maria Kulis, 19.01.2019</p>
            </div>
        </div>
    </div>
</div>

<div class="section padding-bottom-big z-bigger over-hide">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-lg-8">
                <div class="section drop-shadow rounded">
                    <div class="post-box background-dark over-hide insta_img_parent_ext_el">
                        <img src="./Euthenia_files/2.jpg" alt="" class="blog-home-img" data-insta_upload_ext_elem="1">
                        <div class="padding-in">
                            <p class="mt-4">Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit
                                tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta
                                felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id
                                ligula porta felis euismod semper.</p>
                            <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis
                                consectetur purus sit amet fermentum. Donec id elit non mi porta gravida at eget metus.
                            </p>
                            <div class="row mt-4 mb-4">
                                <div class="col-md-6">
                                    <img src="./Euthenia_files/1.jpg" alt="" class="blog-home-img"
                                        data-insta_upload_ext_elem="1">
                                </div>
                                <div class="col-md-6">
                                    <img src="./Euthenia_files/6.jpg" alt="" class="blog-home-img mt-4 mt-md-0"
                                        data-insta_upload_ext_elem="1">
                                </div>
                            </div>
                            <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum
                                nulla sed consectetur. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante
                                venenatis dapibus posuere velit aliquet.</p>
                            <p>You can use the mark tag to <mark>highlight</mark> text.</p>
                            <p><del>This line of text is meant to be treated as deleted text.</del></p>
                            <p><s>This line of text is meant to be treated as no longer accurate.</s></p>
                            <p><ins>This line of text is meant to be treated as an addition to the document.</ins></p>
                            <p><u>This line of text will render as underlined</u></p>
                            <p><small>This line of text is meant to be treated as fine print.</small></p>
                            <p><strong>This line rendered as bold text.</strong></p>
                            <p><em>This line rendered as italicized text.</em></p>
                            <p class="lead">Design must reflect the practical and aesthetic in business but above all...
                                good design must primarily serve people.</p>
                            <p>Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante
                                venenatis dapibus posuere velit aliquet.</p>
                            <div class="video-section hover-target mt-4 mb-4">
                                <figure class="vimeo">
                                    <a href="https://player.vimeo.com/video/289474455" class="insta_img_parent_ext_el">
                                        <img src="./Euthenia_files/2(1).jpg" alt="image" data-insta_upload_ext_elem="1">
                                        <div class="insta_img_icon_wrap_ext_el" title="Send to Direct"
                                            style="top: 15px; left: 15px;">
                                            <div class="insta_img_icon_ext_el"></div>
                                        </div>
                                    </a>
                                </figure>
                            </div>
                            <blockquote class="blockquote">
                                <p>Some people think design means how it looks. But of course, if you dig deeper, it's
                                    really how it works.</p>
                                <footer class="blockquote-footer">Jason Salvatore</footer>
                            </blockquote>
                            <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum
                                nulla sed consectetur. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante
                                venenatis dapibus posuere velit aliquet.</p>
                            <blockquote class="blockquote blockquote-reverse">
                                <p>Some people think design means how it looks. But of course, if you dig deeper, it's
                                    really how it works.</p>
                                <footer class="blockquote-footer">Jason Salvatore</footer>
                            </blockquote>
                            <div class="separator-wrap pt-4 pb-4">
                                <span class="separator"><span class="separator-line dashed"></span></span>
                            </div>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn btn-primary btn-sm ml-0 mr-1 mb-1 hover-target"><span>design</span></a>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn btn-primary btn-sm ml-0 mr-1 mb-1 hover-target"><span>business</span></a>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn btn-primary btn-sm ml-0 mr-1 mb-1 hover-target"><span>responsive</span></a>
                            <div class="separator-wrap pt-4 pb-4">
                                <span class="separator"><span class="separator-line dashed"></span></span>
                            </div>
                            <div class="author-wrap">
                                <img src="./Euthenia_files/t2.jpg" alt="" data-insta_upload_ext_elem="1">
                                <p> by <a href="https://ivang-design.com/euthenia/post.html#"
                                        class="hover-target"><strong>Anna Kulis</strong></a></p>
                            </div>
                        </div>
                        <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;">
                            <div class="insta_img_icon_ext_el"></div>
                        </div>
                    </div>
                </div>
                <div class="section drop-shadow rounded mt-4 over-hide">
                    <div class="post-comm-box background-dark over-hide">
                        <h5 class="mb-3">4 comments</h5>
                        <div class="separator-wrap pt-3 pb-4">
                            <span class="separator"><span class="separator-line dashed"></span></span>
                        </div>
                        <div class="section">
                            <img src="./Euthenia_files/t2.jpg" alt="" data-insta_upload_ext_elem="1">
                            <h6>Anna Kulis <small>April 25, 2017 at 1:03 am</small></h6>
                            <p class="mt-2">Some people think <u>line of text as underlined</u> design means how it
                                looks. <del>This line of text is deleted text.</del> But of course, if you dig deeper,
                                <strong>bold text</strong> it's really how it works. You can use the mark tag to
                                <mark>highlight</mark> text. <em>Italicized text.</em></p>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="ml-0 mr-1 mt-3 mb-1 hover-target">reply</a>
                        </div>
                        <div class="separator-wrap pt-4 pb-4">
                            <span class="separator"><span class="separator-line dashed"></span></span>
                        </div>
                        <div class="section pl-5">
                            <img src="./Euthenia_files/t1.jpg" alt="" data-insta_upload_ext_elem="1">
                            <h6>Marco Furius <small>April 25, 2017 at 1:03 am</small></h6>
                            <p class="mt-2">Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a
                                ante venenatis dapibus posuere velit aliquet.</p>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="ml-0 mr-1 mt-3 mb-1 hover-target">reply</a>
                        </div>
                        <div class="separator-wrap pt-4 pb-4">
                            <span class="separator"><span class="separator-line dashed"></span></span>
                        </div>
                        <div class="section">
                            <img src="./Euthenia_files/t3.jpg" alt="" data-insta_upload_ext_elem="1">
                            <h6>John Doe <small>April 25, 2017 at 1:03 am</small></h6>
                            <p class="mt-2">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis
                                risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus
                                posuere velit aliquet.</p>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="ml-0 mr-1 mt-3 mb-1 hover-target">reply</a>
                        </div>
                        <div class="separator-wrap pt-4 pb-4">
                            <span class="separator"><span class="separator-line dashed"></span></span>
                        </div>
                        <div class="section">
                            <img src="./Euthenia_files/t1.jpg" alt="" data-insta_upload_ext_elem="1">
                            <h6>Marco Kulis <small>April 25, 2017 at 1:03 am</small></h6>
                            <p class="mt-2">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis
                                risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus
                                posuere velit aliquet.</p>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="ml-0 mr-1 mt-3 mb-1 hover-target">reply</a>
                        </div>
                    </div>
                </div>
                <div class="section drop-shadow rounded mt-4 over-hide">
                    <div class="post-comm-box background-dark over-hide ajax-form">
                        <h5 class="pb-3">Leave a comment</h5>
                        <div class="subscribe-box mt-3">
                            <input type="text" value="" placeholder="Your Name *" class="hover-target">
                        </div>
                        <div class="subscribe-box mt-4">
                            <input type="text" value="" placeholder="Email *" class="hover-target">
                        </div>
                        <div class="subscribe-box mt-4">
                            <input type="text" value="" placeholder="Website" class="hover-target">
                        </div>
                        <div class="subscribe-box mt-4">
                            <textarea name="message" placeholder="Comment *" class="hover-target"></textarea>
                        </div>
                        <button class="btn-long mt-4 hover-target" type="button">submit comment</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mt-4 mt-lg-0">
                <div class="sidebar-box background-dark drop-shadow rounded">
                    <div class="subscribe-box ajax-form">
                        <input type="text" value="" placeholder="type here" class="hover-target">
                        <button class="subscribe-1 hover-target" type="submit" value="">
                            search
                        </button>
                    </div>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Categories</h6>
                    <ul class="list-style circle pl-4 pb-0">
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">entrepreneurship</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">design</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">small business</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">media</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">marketing</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">agency</a>
                        </li>
                    </ul>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Latest Video</h6>
                    <div class="video-section hover-target mt-4 mb-4">
                        <figure class="vimeo">
                            <a href="https://player.vimeo.com/video/289474455" class="">
                                <img src="./Euthenia_files/2(1).jpg" alt="image" data-insta_upload_ext_elem="1">
                            </a>
                        </figure>
                    </div>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Text Widget</h6>
                    <p class="pb-0">Design must reflect the practical and aesthetic in business but above all... good
                        design must primarily serve people.</p>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Latest News</h6>
                    <ul class="list-style circle-o pl-4 pb-0">
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html"
                                class="btn-link btn-primary hover-target pl-0">Don’t get lost.</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html"
                                class="btn-link btn-primary hover-target pl-0">Moments from a life.</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html"
                                class="btn-link btn-primary hover-target pl-0">Modern webdesign.</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html"
                                class="btn-link btn-primary hover-target pl-0">Don´t give up...</a>
                        </li>
                    </ul>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Tags</h6>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">design</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">business</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">responsive</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">video</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">audio</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">clean</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">bootstrap</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">awesome</a>
                    <a href="https://ivang-design.com/euthenia/post.html#"
                        class="tags-link hover-target btn-sm ml-0 mr-1 mb-1">creative</a>
                    <div class="separator-wrap my-5">
                        <span class="separator"><span class="separator-line dashed"></span></span>
                    </div>
                    <h6 class="mb-3">Archives</h6>
                    <ul class="list-style circle-o pl-4 pb-0">
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">august 2017</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">july 2017</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">june 2017</a>
                        </li>
                        <li>
                            <a href="https://ivang-design.com/euthenia/post.html#"
                                class="btn-link btn-primary hover-target pl-0">may 2017</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
    <div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
        data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">Story</div>
    <div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="2"
        data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
        <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines">
                    <a href="https://ivang-design.com/euthenia/post.html" class="hover-target animsition-link">
                        <h4>Older Post</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
        $("body").removeClass("over-hide");
</script>
@endsection

