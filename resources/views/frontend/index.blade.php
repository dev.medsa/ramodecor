@extends('layouts.frontend')
    @section('content')
    
    <!-- Primary Page Layout
		================================================== -->

<div class="section full-height over-hide swiper-container z-bigger swiper-container-horizontal swiper-container-free-mode"
id="hero-slider" style="cursor: grab;">
<ul class="swiper-wrapper case-study-wrapper" style="transform: translate3d(0px, 0px, 0px);">
    <li class="swiper-slide full-height case-study-name swiper-slide-active active">
        <a href="{{ route('frontend.category', []) }}" class="hover-target animsition-link">
            <h1>دکوراسیون داخلی</h1>
        </a>
    </li>
    <li class="swiper-slide full-height case-study-name swiper-slide-next">
        <a href="https://ivang-design.com/euthenia/project-1.html" class="hover-target animsition-link">
            <h1>تعمیرات مبلمان</h1>
        </a>
    </li>
</ul>

<!-- Background Images -->
<div class="swiper-scrollbar">
    <div class="swiper-scrollbar-drag" style="transform: translate3d(0px, 0px, 0px); width: 19px;"></div>
</div>
<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
</div>

<!-- Slider background images 
    ================================================== -->

<ul class="case-study-images">
<li class="show">
    <div class="img-hero-background-over no-blur" style="background-image: url(&#39;images/1.jpg&#39;);"></div>
    <p class="fullscreen-image no-blur">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
        طراحان گرافیک است.</p>
    <div class="hero-number">01</div>
    <div class="hero-number-fixed">02</div>
</li>
<li class="">
    <div class="img-hero-background-over no-blur" style="background-image: url('/images/2.jpg');"></div>
    <p class="fullscreen-image no-blur">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
        طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
        تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
        گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان
        رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که
        تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی
        دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
    <div class="hero-number">02</div>
</li>
</ul>

    @endsection