@extends('layouts.frontend')
    @section('content')
    <div class="shadow-title parallax-top-shadow" style="top: -19.5px;">Decor</div>
		
		<div class="section padding-page-top padding-bottom over-hide z-bigger">
			<div class="container z-bigger">
				<div class="row page-title px-5 px-xl-2">
					<div class="col-12 parallax-fade-top" style="top: 19.5px; opacity: 0.87;">
						<h1>دکور داخلی</h1>
					</div>
					<div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 19.5px; opacity: 0.87;">
						<p class="text-secondary">we build great brands</p>
					</div>
				</div>
			</div>
		</div>

		<div class="section padding-bottom-big over-hide z-bigger">
			<div class="container">
				<div class="row px-5 px-xl-2">
					<div class="col-12">
						<div class="img-wrap insta_img_parent_ext_el img-mask ">
							<div class="hero"></div>
						<div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
					</div>	
					<div class="col-12 padding-top">
						<h3 >What we do</h3>	
					</div>
					<div class="col-12 mt-2 mb-5 page-title-small">
						<p class="mb-0 pb-0 text-secondary">stuff to note</p>	
					</div>	
					<div class="col-md-4" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">	
						<ul class="studio-list">
							<li><p class="text-secondary">Banner creation</p></li>
							<li><p class="text-secondary">Media campaigns</p></li>
							<li><p class="text-secondary">Photography</p></li>
							<li><p class="text-secondary">Copywritting</p></li>
							<li><p class="text-secondary">Prototyping</p></li>
						</ul>	
					</div>	
				
				</div>		
			</div>				
		</div>

		<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer anim">
			<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="5" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">Say Hello</div>
			<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="6" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
				<div class="row">
					<div class="col-12 text-center z-bigger py-5">
						<div class="footer-lines">
							<a href="contact.html" class="hover-target animsition-link"><h4>Have a Project?</h4></a>
						</div>
					</div>
				</div>
			</div>
		</div>	
	<script>
		$( "body" ).removeClass( "over-hide" );
	</script>

	@endsection