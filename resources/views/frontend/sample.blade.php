@extends('layouts.frontend')
@section('content')
<div class="shadow-title parallax-top-shadow" style="top: -65px;">Our Work</div>

<div class="section padding-page-top padding-bottom over-hide z-bigger">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12 parallax-fade-top" style="top: 65px; opacity: 0.566667;">
                    <h1>Our Work</h1>
            </div>
            <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 65px; opacity: 0.566667;">
                <p>we do magic</p>
            </div>
        </div>
    </div>
</div>

<div class="section padding-bottom-big over-hide z-bigger">
    <div class="container">
        <div class="row px-5 px-xl-2">
            <div class="col-md-6 img-slice-wrap mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project.html"
                    class="hover-target animsition-link hover-portfolio-box">
                    <div class="scroll-img uncover" style="background-image: url(/images/1.jpg);">
                        <div class="uncover__img"
                        style="background-image: url(&quot;/images/1.jpg&quot;); transform: scale(1);"></div>
                        <div class="uncover__slices uncover__slices--vertical">
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(-100%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(-100%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(-100%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(-100%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(-100%);"></div>
                        </div>
                    </div>
                    <p>photography</p>
                    <h4>Dark Dream</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-3 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project-1.html"
                class="hover-target animsition-link hover-portfolio-box">
                <div class="scroll-img uncover" style="background-image: url(/images/2.jpg);">
                        <div class="uncover__img"
                        style="background-image: url(&quot;/images/2.jpg&quot;); transform: scale(1.03748);"></div>
                        <div class="uncover__slices uncover__slices--horizontal">
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            </div>
                    </div>
                    <p>fashion</p>
                    <h4>L'Etoile Feb</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-6 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project.html"
                class="hover-target animsition-link hover-portfolio-box">
                <div class="scroll-img uncover" style="background-image: url(/images/3.jpg);">
                        <div class="uncover__img"
                        style="background-image: url(&quot;/images/3.jpg&quot;); transform: scale(1.21948);"></div>
                        <div class="uncover__slices uncover__slices--horizontal">
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                        </div>
                    </div>
                    <p>art direction</p>
                    <h4>Tempo</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-3 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project-1.html"
                class="hover-target animsition-link hover-portfolio-box">
                    <div class="scroll-img uncover" style="background-image: url(/images/4.jpg);">
                        <div class="uncover__img"
                            style="background-image: url(&quot;/images/4.jpg&quot;); transform: scale(1.3);"></div>
                        <div class="uncover__slices uncover__slices--vertical">
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                        </div>
                    </div>
                    <p>advertising</p>
                    <h4>Kresios</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project.html"
                class="hover-target animsition-link hover-portfolio-box">
                    <div class="scroll-img uncover" style="background-image: url(/images/5.jpg);">
                        <div class="uncover__img"
                            style="background-image: url(&quot;/images/5.jpg&quot;); transform: scale(1.0647);"></div>
                            <div class="uncover__slices uncover__slices--vertical">
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            </div>
                    </div>
                    <p>fashion</p>
                    <h4>The Battle</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-3 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project-1.html"
                class="hover-target animsition-link hover-portfolio-box">
                <div class="scroll-img uncover" style="background-image: url(/images/6.jpg);">
                    <div class="uncover__img"
                    style="background-image: url(&quot;/images/6.jpg&quot;); transform: scale(1);"></div>
                    <div class="uncover__slices uncover__slices--horizontal">
                        <div class="uncover__slice"
                        style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                        <div class="uncover__slice"
                        style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                        </div>
                    </div>
                    <p>architecture</p>
                    <h4>Hospitality</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-6 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project.html"
                    class="hover-target animsition-link hover-portfolio-box">
                    <div class="scroll-img uncover" style="background-image: url(/images/7.jpg);">
                        <div class="uncover__img"
                            style="background-image: url(&quot;/images/7.jpg&quot;); transform: scale(1);"></div>
                            <div class="uncover__slices uncover__slices--vertical">
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            </div>
                    </div>
                    <p>digital art</p>
                    <h4>Processors</h4>
                </a>
            </div>
            <div class="section clearfix"></div>
            <div class="offset-md-3 col-md-6 img-slice-wrap mt-5 mb-5 over-hide">
                <a href="https://ivang-design.com/euthenia/project-1.html"
                class="hover-target animsition-link hover-portfolio-box">
                    <div class="scroll-img uncover" style="background-image: url(/images/8.jpg);">
                        <div class="uncover__img"
                        style="background-image: url(&quot;/images/8.jpg&quot;); transform: scale(1);"></div>
                        <div class="uncover__slices uncover__slices--horizontal">
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                                <div class="uncover__slice"
                                style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                            <div class="uncover__slice"
                            style="color: rgb(255, 255, 255); transform: translateX(0%) translateY(0%);"></div>
                        </div>
                    </div>
                    <p>architecture</p>
                    <h4>Mrsoet</h4>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
    <div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"
        data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">Say Hello
    </div>
    <div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="2"
    data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
    <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines">
                    <a href="https://ivang-design.com/euthenia/contact.html" class="hover-target animsition-link">
                        <h4>Get in Touch</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("body").removeClass("over-hide");
    </script>
    @push('script')    
        <script src="{{ asset('js/uncover.js') }}"></script>
        <script src="{{ asset('js/sliceRevealer.js') }}"></script>
    @endpush
@endsection