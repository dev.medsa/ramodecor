@extends('layouts.frontend')
@section('content')


<div class="shadow-title parallax-top-shadow" style="top: 0px;">The Battle</div>
		
<div class="section padding-page-top padding-bottom over-hide z-bigger">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12 parallax-fade-top" style="top: 0px; opacity: 1;">
                <h1>The Battle</h1>
            </div>
            <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3" style="top: 0px; opacity: 1;">
                <p>fashion, photography</p>
            </div>
        </div>
    </div>
</div>

<div class="section padding-bottom-big over-hide z-bigger">
    <div class="container">
        <div class="row px-5 px-xl-2 justify-content-center">
            <div class="col-12 mb-5">
                <div class="img-wrap insta_img_parent_ext_el">
                    <img src="./Euthenia_files/1.jpg" alt="" data-insta_upload_ext_elem="1">
                <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
            </div>	
            <div class="col-lg-8 mb-5 text-center">
                <p class="mb-0 pb-0 lead">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni.</p>
            </div>
        </div>		
    </div>	
    <div class="container-fluid">
        <div class="row px-5 px-xl-2 px-xl-0">
            <div class="col-xl-6 px-xl-0 align-self-center">
                <div class="row mt-5 mt-xl-0 justify-content-center">
                    <div class="offset-xl-5 col-sm-8 col-md-6 col-lg-5 text-center text-xl-right" data-scroll-reveal="enter left move 30px over 0.5s after 0.1s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                        <h5 class="mb-3">art direction</h5>
                        <p class="mb-0 pb-0">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>	
                    </div>
                    <div class="offset-xl-5 col-sm-8 col-md-6 col-lg-5 mt-5 mt-md-0 mt-xl-5 text-center text-xl-right" data-scroll-reveal="enter left move 30px over 0.5s after 0.1s" data-scroll-reveal-id="2" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                        <h5 class="mb-3">photography</h5>
                        <p class="mb-0 pb-0">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                    </div>
                    <div class="offset-xl-5 col-sm-8 col-md-6 col-lg-5 mt-5 text-center text-xl-right" data-scroll-reveal="enter left move 30px over 0.5s after 0.1s" data-scroll-reveal-id="3" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                        <h5 class="mb-3">cinematography</h5>
                        <p class="mb-0 pb-0">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                    </div>
                    <div class="offset-xl-5 col-sm-8 col-md-6 col-lg-5 mt-5 text-center text-xl-right" data-scroll-reveal="enter left move 30px over 0.5s after 0.1s" data-scroll-reveal-id="4" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                        <h5 class="mb-3">advertising</h5>
                        <p class="mb-0 pb-0">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 px-xl-0 order-first order-xl-last">
                <div class="img-wrap insta_img_parent_ext_el">
                    <img src="./Euthenia_files/2.jpg" alt="" data-insta_upload_ext_elem="1">
                <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
            </div>	
        </div>		
    </div>	
    <div class="container">
        <div class="row px-5 px-xl-2 justify-content-center">
            <div class="col-md-6 mt-5">
                <div class="img-wrap insta_img_parent_ext_el">
                    <img src="./Euthenia_files/4.jpg" alt="" data-insta_upload_ext_elem="1">
                <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
            </div>	
            <div class="col-md-6 mt-5">
                <div class="img-wrap insta_img_parent_ext_el">
                    <img src="./Euthenia_files/5.jpg" alt="" data-insta_upload_ext_elem="1">
                <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
            </div>
            <div class="col-lg-8 mt-5 text-center" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="5" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <p class="mb-0 pb-0 lead">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni.</p>
            </div>
            <div class="col-12 mt-5" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="6" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <div class="img-wrap insta_img_parent_ext_el">
                    <img src="./Euthenia_files/3.jpg" alt="" data-insta_upload_ext_elem="1">
                <div class="insta_img_icon_wrap_ext_el" title="Send to Direct" style="top: 15px; left: 15px;"><div class="insta_img_icon_ext_el"></div></div></div>	
            </div>	
        </div>		
    </div>			
</div>

<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
    <div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s" data-scroll-reveal-id="7" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">L'Etoile Feb</div>
    <div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s" data-scroll-reveal-id="8" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
        <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines">
                    <a href="https://ivang-design.com/euthenia/project-1.html" class="hover-target animsition-link"><h4>Next Project</h4></a>
                </div>
            </div>
        </div>
    </div>		
</div>
<script>
        $("body").removeClass("over-hide");
</script>
@endsection

